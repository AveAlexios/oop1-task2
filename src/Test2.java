public class Test2 {
    int variableOne;
    int variableTwo;

    public Test2() {
        variableOne = 1;
        variableTwo = 2;
    }

    public Test2(int _variableOne, int _variableTwo) {
        variableOne = _variableOne;
        if (this.variableOne == 0){
            throw new RuntimeException("Your variable is zero");
        }
        variableTwo = _variableTwo;
    }

    public int getVariableOne(){
        return variableOne;
    }

    public void setVariableOne(int variableOne){
        this.variableOne = variableOne;
    }

    public int getVariableTwo() {
        return variableTwo;
    }

    public void setVariableTwo(int variableTwo) {
        this.variableTwo = variableTwo;
    }
}

